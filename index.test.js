"use strict";

const expect = require("chai").expect;
const createRandom = require("./index").default;

module.exports = function (suite) {
	suite.test("When seed is not a number, then throw.", test => {
		expect(()=>createRandom("Hi")).to.throw();
	});

	suite.test("When seed is a negative number, then throw.", test => {
		expect(()=>createRandom(-1)).to.throw();
	});

	suite.test("Integrity - Same seed generates the same values.", test => {
		for (let seed = 0; seed < 1e8; seed = 1 + seed * 5) {
			let random1 = createRandom(seed);
			let random2 = createRandom(seed);

			let checkCount = 1e6;

			for (let i = 0; i < checkCount; i++) {
				let a = random1();
				let b = random2();

				if (a !== b) {
					throw new Error(`Integrity fail. Seed: ${seed}. ${a} != ${b}`);
				}
			}
		}
	});

	suite.test("Spread - values are spreaded equally.", test => {
		let fromSeed = 0;
		let toSeed = 100;

		let arrayCheckSize = 100;
		let randomCount = arrayCheckSize * 1000;

		for (let seed = fromSeed; seed < toSeed; seed++) {
			let random = createRandom(seed);
			let values = []; for (let i = 0; i < arrayCheckSize; i++) values.push(0);

			for (let i = 0; i < randomCount; i++) {
				let value = random();
				values[Math.floor(value * values.length)]++;
			}

			let average = randomCount / values.length;

			for (let value of values) {
				if (Math.abs(value - average) / average > 0.25) {
					console.log("Shit", seed, values);
					break;
				}
			}
		}
	});

	suite.test("Integrity - Same seed generates the same values.", test => {
		const count = 1e7;
		const random = createRandom();

		const start = performance.now();

		for (let i = 0; i < count; i++) {
			random();
		}

		const end = performance.now();

		if (end - start > 250) {
			throw new Error("Too slow.");
		}
	});
}