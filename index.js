"use strict";

if (typeof module === "undefined") {
	var module = { exports: {} };
	window.itayRandom = module.exports;
}

module.exports.default = module.exports.createRandom = function (seed) {
	if (typeof seed === "undefined") {
		seed = (new Date()).getTime();
	}
	else if (typeof seed !== "number" || seed < 0) {
		throw new Error("The given seed should be a non-negative number.");
	}

	seed = seed % 1e8;
	seed += 1e7;

	let state = { lastResult: seed };

	const multiplyer = 10.1111111111;

	return function () {
		var temp = state.lastResult * multiplyer;
		temp = temp - (temp | 0);

		state.lastResult = temp;

		return temp;
	};
}