# itay-random
A simple quick seedable random.

## Install
`npm install --save itay-random`

## Basic Usage
```js
const createRandom = require("./index").default;

const random = createRandom(17);
let value1 = random(); // 0.9998888820409775 (Always the first value of seed 17)
let value2 = random(); // 0.10998758506988437 (Always the second value of seed 17)

const randomSeed = createRandom(); // Uses 'new Date()' as a seed.
let randomSeedValue = randomSeed(); // Changes every run.
```

## Performance
Faster than the native Math.random().